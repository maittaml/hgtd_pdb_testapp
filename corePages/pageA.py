### standard
import streamlit as st
from core.Page import Page
from PIL import Image
### custom
import datetime
import os
import sys
import json
import subprocess
import ast
### PDB stuff
import itkdb
import core.DBaccess as DBaccess
import core.stInfrastructure as infra

#####################
### Top page
#####################

### format datetime
def DateFormat(dt):
    return str("{0:02}-{1:02}-{2:04}".format(dt.day,dt.month,dt.year))+" at "+str("{0:02}:{1:02}".format(dt.hour,dt.minute))


#def GetCodes(args):
    #code1,code2=None,None
    #for a in args:
        #if "ac1" in a: code1=a[4::].strip('"').strip("'")
        #if "ac2" in a: code2=a[4::].strip('"').strip("'")
    #return code1,code2

#####################
### main part
#####################

class Pagea(Page):
    def __init__(self):
        super().__init__("Home", "HGTDPDB Web Application", ['nothing to report'])

    def main(self):
        super().main()
        nowTime = datetime.datetime.now()
        st.write("""### :calendar: ("""+DateFormat(nowTime)+""")""")
        st.title('Welcome to the HGTD Production Database Web Application ')
        st.write('Web application to interface with the database')
        image = Image.open('/home/malak/HGTDPDB-webapp/media/HGTD.png')
        st.image(image, use_column_width=True)

        

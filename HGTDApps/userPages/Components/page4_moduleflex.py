### standard
import streamlit as st
from core.Page import Page
### custom
import pandas as pd
import ast
import csv
import json
import plotly.express as px
from datetime import datetime
### PDB stuff
import itkdb
import core.DBaccess as DBaccess
import core.stInfrastructure as infra
import itkdb
import itkdb.exceptions as itkX

#####################
### useful functions
#####################
def ColorCells(s, df, colName, flip=False):
    thisRow = pd.Series(data=False, index=s.index)
    colours=['red','blue','green','orange','purple''yellow','pink','lightblue','lightgreen']*3
    names=list(df[colName].unique())
    if flip:
        return ['background-color: %s ; color: %s'% ('white',colours[names.index(s[colName])])]*len(df.columns)
    else:
        return ['background-color: %s ; color: %s'% (colours[names.index(s[colName])],'black')]*len(df.columns)


infoList=["  * upload IZM _xlsx_ data file",
        "  * review test schema",
        "   * reset if required",
        "   * edit if required",
        "  * upload test schema",
        "   * delete test upload if required"]
#####################
### main part
#####################

class Page4(Page):
    def __init__(self):
        super().__init__("moduleflex", ":microscope: Moduleflex data", ['nothing to report'])

    def main(self):
        super().main()
        df = pd.read_csv("/home/malak/HGTDPDB-webapp/modules/data/moduleflexdata.csv")
        #st.title("moduleflex data")  # add a title
        st.write(df)  # visualize my dataframe in the Streamlit app


        

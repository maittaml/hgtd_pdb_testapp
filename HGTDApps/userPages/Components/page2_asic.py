### standard
import streamlit as st
from core.Page import Page
### custom
import pandas as pd
import ast
import csv
import json
import plotly.graph_objects as go
from datetime import datetime
### PDB stuff
import itkdb
import core.DBaccess as DBaccess
import core.stInfrastructure as infra
import itkdb
import itkdb.exceptions as itkX

#####################
### useful functions
#####################

### main part
#####################

class Page2(Page):
    def __init__(self):
        super().__init__("asic", ":microscope: ASIC data", ['nothing to report'])

    def main(self):
        super().main()
        df = pd.read_csv("/home/malak/HGTDPDB-webapp/modules/data/asicdata.csv")
        st.title("ASIC data")  # add a title
        st.write(df)  # visualize my dataframe in the Streamlit app


### standard
import streamlit as st
from core.Page import Page
### custom
import pandas as pd
import plotly.graph_objects as go
import ast
import csv
import os
### PDB stuff
import itkdb
import core.DBaccess as DBaccess
import core.stInfrastructure as infra
import itkdb
import itkdb.exceptions as itkX

#####################
### useful functions
#####################

#infoList=["  * input componentType",
   #     "   * Get workflow: stages and tests (sankey plot)"]
#####################
### main part
#####################

class Page1(Page):
    def __init__(self):
        super().__init__("sensor", ":microscope: Sensor data", ['nothing to report'])

    def main(self):
        super().main()

        df = pd.read_csv("/home/malak/HGTDPDB-webapp/modules/data/sensord.csv") 
        st.title("sensor data")  # add a title
        st.write(df)  # visualize my dataframe in the Streamlit app
## drag and drop .csv files
        uploaded_file = st.file_uploader("Choose a file", type=["csv","dat"])
        if uploaded_file is not None:
          df = pd.read_csv(uploaded_file)
          st.write(df)
        else:
            st.write("No data file set")
            filePath=os.path.realpath(__file__)
            exampleFileName="data/sensord.csv"
                #st.write("looking in:",filePath[:filePath.rfind('/')])
            st.download_button(label="Download example", data=pd.read_csv(filePath[:filePath.rfind('/')]+"/"+exampleFileName).to_csv(index=False),file_name=exampleFileName)         

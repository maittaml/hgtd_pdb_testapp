### standard
import streamlit as st
from core.Page import Page
### custom
import pandas as pd
import plotly.graph_objects as go
import ast
import csv
#####################
### main part
#####################

class Page5(Page):
    def __init__(self):
        super().__init__("module", ":microscope: Module data", ['nothing to report'])

    def main(self):
        super().main()

        df = pd.read_csv("/home/malak/itk_pdb_testapp/userPages/Components/data/moduledata.csv")
        #st.title("module  data")  # add a title
        st.write(df)  # visualize my dataframe in the Streamlit app


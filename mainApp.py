from core.MultiApp import App

smalls={
    'git':"https://gitlab.cern.ch/maittaml/hgtd_pdb_testapp",
    'other':"otherstuff"
}

myapp = App("hgtdTestApp", "Streamlit HGTD  PDB Test App", smalls)

myapp.main()

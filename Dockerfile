# base image
#FROM python:3.8
FROM kwraight/itk-multi-app:cern

# exposing default port for streamlit
# EXPOSE 8501 

# copying new pages over
USER root
RUN rm -rf ./userPages/theme*
COPY  hgtd_pdb_testapp/HGTDApps/userPages/* ./userPages/HGTDApps/
# set main
COPY  mainFiles/mainApphgtd.py ./mainApp.py
RUN sed -i 's/BUILDDATE/'$(date +%d-%m-%y)'/g' mainApp.py
# python packages
COPY requirements.txt ./requirements.txt
RUN pip3 install -r requirements.txt

# fudge for CERN image
RUN mkdir /.streamlit
COPY temp_id  /.streamlit/.stable_random_id
RUN chmod 777 /.streamlit/.stable_random_id

# run as not root
RUN adduser appuser
USER appuser

# match exposed port
CMD ["streamlit", "run", "mainApp.py","--server.port=8501"]


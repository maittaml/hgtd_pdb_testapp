### standard
import streamlit as st
from core.Page import Page
### custom
import pandas as pd
import ast
import csv
import json
import plotly.express as px
from datetime import datetime
#####################
### main part
#####################

class Page7(Page):
    def __init__(self):
        super().__init__("support unit", ":microscope: Support unit data", ['nothing to report'])

    def main(self):
        super().main()

        df = pd.read_csv("/home/malak/itk_pdb_testapp/userPages/Components/data/supportunitdata.csv")
        #st.title("module  data")  # add a title
        st.write(df)  # visualize my dataframe in the Streamlit app


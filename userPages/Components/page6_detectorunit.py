### standard
import streamlit as st
from core.Page import Page
### custom
import pandas as pd
import ast
import csv
import json
import plotly.express as px
from datetime import datetime

#####################
### main part
#####################

class Page6(Page):
    def __init__(self):
        super().__init__("detector unit", ":microscope: Detector unit data", ['nothing to report'])

    def main(self):
        super().main()

        df = pd.read_csv("/home/malak/itk_pdb_testapp/userPages/Components/data/detectorunitdata.csv")
        #st.title("module  data")  # add a title
        st.write(df)  # visualize my dataframe in the Streamlit app


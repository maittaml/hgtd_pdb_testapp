### standard
import streamlit as st
from core.Page import Page
### custom
import pandas as pd
import ast
import csv
import json
import plotly.express as px
from datetime import datetime


#####################
### useful functions
#####################

#####################
### main part
#####################

class Page3(Page):
    def __init__(self):
        super().__init__("hybrid", ":microscope: Hybrid data", ['nothing to report'])

    def main(self):
        super().main()
        df = pd.read_csv("/home/malak/HGTDPDB-webapp/modules/data/hybriddata.csv")  # read a CSV file inside the 'data" folder next to 'app.py'
# df = pd.read_excel(...)  # will work for Excel files

        #st.title("hybrid data")  # add a title
        st.write(df)  # visualize my dataframe in the Streamlit app

